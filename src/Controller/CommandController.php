<?php

namespace App\Controller;

use App\Entity\Command;
use App\Repository\CommandRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandController extends AbstractController
{
    /**
     * @Route("/command", name="command")
     */
    public function index(CommandRepository $commandRepository, Request $request)
    {
        $command = $commandRepository->findAll();
        return $this->render('command/index.html.twig', [
            'commandes' => $command,
        ]);
    }

    /**
     * @Route("/command/{id}", name="command.show")
     */
    public function show(CommandRepository $commandRepository,$id)
    {
        $commande = $commandRepository->find($id);

        if (!$commande) {
            throw $this->createNotFoundException('The article does not exist');
        }

        return $this->render('command/show.html.twig', [
            'commande' => $commande,
            'nbProduct' => $commande->getNbProduct(),
            'prixtotal' => $commande->getPrice(),
        ]);
    }
}
