<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Product;
use App\Form\CommandType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(SessionInterface $session,Request $request,ProductRepository $productRepository)
    {
        $command = new Command();
        $panier = $session->get('panier', []);
        $total=0;

        $panierForm = $this->createForm(CommandType::class, $command);
        $panierForm->handleRequest($request);

        if ($panierForm->isSubmitted() && $panierForm->isValid()) {
            $command->setCreatedAt(new \DateTime);
            foreach($panier as $id=>$quantity){
                $command->addProduct($productRepository->find($id));
            }
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($command);  
            $manager->flush();
            $session->clear();
            $this->addFlash('success',"La commande est validée !");
            return $this->redirectToRoute('panier');
        }

        foreach($panier as $key => $produit) {
            $total = $total + $produit->getPrice();
        }
        return $this->render('panier/index.html.twig', [
            'panier' => $panier,
            'total' => $total,
            'panierForm' => $panierForm->createView() 
        ]);
    }


    /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add($id, SessionInterface $session,Product $product,ProductRepository $productRepository)
    {
        if ($product) {
            $panier = $session->get('panier', []);

            if($panier){
                $panier[$id] = $productRepository->find($id);
                $this->addFlash('success',"La produit est ajouté !");
            }else{
                $pannier=array();
                $panier[$id] = $productRepository->find($id);
                $this->addFlash('success',"La produit est ajouté !");
            }
            $session->set('panier', $panier);
            return $this->json('ok', 200);
        }else{
            return $this->json('nok', 404);
        }
    }

    /**
     * @Route("/panier/delete/{id}", name="panier.delete")
     */
    public function delete($id, SessionInterface $session,Product $product,ProductRepository $productRepository,Request $request)
    {
        if ($product) {
            $panier = $session->get('panier', []);
            $csrfToken = $request->request->get('token');

            if ($this->isCsrfTokenValid('delete-item', $csrfToken)) {
                if($panier){
                    unset($panier[$id]);
                    $session->set('panier', $panier);
                    $this->addFlash('danger',"La produit est suprimé !");
                }
                return $this->redirectToRoute('panier');
            }else{
                throw new InvalidCsrfTokenException();
            }
        }
    }
}
