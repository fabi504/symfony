<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProductRepository $productRepository)
    {
        return $this->render('home/index.html.twig', [
            'PriceLess' => $productRepository->findPriceLess(5),
            'mostRecentProducts' => $productRepository->findMostRecent(5)
        ]);
    }
}
