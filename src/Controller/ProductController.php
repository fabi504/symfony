<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index(ProductRepository $productRepository,Request $request)
    {
        $produits = $productRepository->findAll();

        return $this->render('product/index.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("/product/{id}", name="product.show")
     */
    public function show(ProductRepository $productRepository,$id)
    {
        $produit = $productRepository->find($id);

        if (!$produit) {
            throw $this->createNotFoundException('The article does not exist');
        }

        return $this->render('product/show.html.twig', [
            'produit' => $produit,
        ]);
    }
}
